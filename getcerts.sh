#!/usr/bin/env bash

function getCertificates() {
	rsync -Pav -e "ssh -i $3 -o StrictHostKeyChecking=no" ubuntu@$1:/mnt/nomad/certs $2
	ssh -i $3 ubuntu@$1 "export VAULT_ADDR=http://127.0.0.1:8200 && vault kv get -field=value acltoken/bootstrap" | tee certs/acl.token
}

function main() {
	ip=$1
	key=$2
	# Fetching certificates
	rm -r ./certs || true
	mkdir -p ./certs
	echo $ip
	getCertificates $ip . $key
	export NOMAD_ADDR=http://$ip:4646
	export NOMAD_CACERT=$(pwd)/certs/ca.crt
	export NOMAD_CLIENT_CERT=$(pwd)/certs/cli.crt
	export NOMAD_CLIENT_KEY=$(pwd)/certs/cli.key
	export NOMAD_TOKEN=$(cat ./certs/acl.token)
}

main $1 $2
