# Hashistacc Nomad Sample

You'll need to fill your `datacenters` names in the appropriate field of each job file.

# Fabio
Launches 1 fabio instance on each "nomad client" (reachable at fabio.<datacenterN>.<domain.com>:9998).

# Prometheus
Launches 1 prometheus instance (across all regions, reachable in your *first* DC at fabio.dc1.<domain.com>:9999)

# HTTP-echo
Sample web-app for testing purposes.

# IPFS
Launches a `count` number of IPFS nodes on the public IPFS network.

# IPFS-cluster
Attempt to pair an IPFS daemon and a ipfs-cluster-service instance in a same however. Currently meeting the common "peer 0" challenge.
/!\ Currently the cluster secret is being hard-coded. Automated secret generation + storage in Vault is WIP.

# Ethereum
Launches a `count` number of Ethereum nodes.
However, the default configuration of Hashistacc in regards to disk space isn't sufficient for a node to fully sync.

# Scaling scenarios

## Scale-out scenario
If you do modify the `./jobs/http-echo.hcl` to increase the `count` to `4` and you run a `nomad run jobs/http-echo.hcl`. As the job is configured to run on a unique client, and the amount of "nomad clients" was 3, you'll observe a new "nomad client" instance being bootstrapped after 5 minutes.

## Scale-in scenario
If you then modify the `count` of the same job back to `3` (and apply it with `nomad job plan ./jobs/http-echo.hcl` and `nomad job run ./jobs/http-echo.hcl`), the inactive instance will automatically shut down after a period of around 5 minutes.

# Helper script

The `getcerts.sh` script allows you to seemlessly communication with your Nomad cluster by grabbing the TLS certificates as well as a Nomad ACL token from Vault. 
Run:
`source getcerts.sh <NOMAD-SERVER-IP-ADDRESS> <SSH-KEY-PATH>`
And your current shell environment will be populated with the necessary variables to reach the Nomad cluster and run jobs. 