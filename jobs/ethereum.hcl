job "test" {
  datacenters = ["dc1","dc2"]
  region = "global"

  group "ethereum" {
    count = 1
    task "ethereum-node" {
      driver = "docker"

      config {
        image   = "ethereum/client-go"
        args = [
          "--syncmode", "fast",
          "--ws",
          "--wsaddr", "0.0.0.0",
          "--wsorigins=\"*\"",
          "--datadir", "/geth",
          "--ipcdisable",
          "--verbosity", "4"

        ]
        volumes = [
          "/opt/geth:/geth"
        ]

        port_map {
          p2p = "30303",
          websocket = "8546"
        }
      }

      resources {
        network {
          mode = "bridge"
          port "p2p" {
            static = 30303
            to     = 30303
          },
          port "websocket" {
            static = 8546
            to = 8546
          }
        }
      }
      service {
        name = "ethereum"
        tags = ["eth"]
        port = "p2p"
        check {
          name     = "alive"
          type     = "tcp"
          interval = "30s"
          timeout  = "5s"
        }
        connect {
          sidecar_service {}
        }
      }
    }
  }

/**
  group "chainlink" {
    count = 1
    task "chainlink-node" {
      driver = "docker"

      env {
        ROOT = /chainlink
        LOG_LEVEL = debug
        ETH_CHAIN_ID = 1
        CHAINLINK_TLS_PORT = 0
        SECURE_COOKIES = false
        ALLOW_ORIGINS = *
        ETH_URL = "ws://${NOMAD_UPSTREAM_ADDR_ethereum}"
      }      

      template {
        change_mode = "noop"
        destination = ".chainlink/.password"
        data = <<EOH
12345
EOH
      template {
        change_mode = "noop"
        destination = ".chainlink/.api"
        data = <<EOH
sadjy.sadjan@gmail.com
12345
EOH

      config {
        image   = "smartcontract/chainlink"
        args = [
          "local", "n",
          "-p", "/chainlink/.password", "-a", "/chainlink/.api"
        ]
        volumes = [
          ".chainlink:/chainlink"
        ]

        port_map {
          ui = "6688"
        }
      }

      network {
        mode = "host"

        port "ui" {
          static = 6688
        }
      }
    
      service {
        name = "chainlink"
        tags = ["link"]
        port = "ui"
        check {
          name     = "alive"
          type     = "tcp"
          interval = "30s"
          timeout  = "5s"
        }
        connect {
          sidecar_service {
            proxy {
              upstreams {
                destination_name = "ethereum"
                local_bind_port  = 8546
              }
            }
          }
        }
      }
    }
  }
**/
}