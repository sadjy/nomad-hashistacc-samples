job "ipfs" {
  datacenters = ["dc1"]
  region = "global"

  group "ipfs" {
    count = 2
    
    constraint {
      operator  = "distinct_hosts"
      value     = "true"
    }

    task "ipfs-daemon" {
      driver = "docker"

      config {
        image   = "ipfs/go-ipfs" 
        volumes = [
          "/opt/ipfs_exports:/export",
          "/opt/ipfs_data:/data/ipfs"
        ]
        port_map {
          api = "4001"
          webui = "5001"
          http = "8080"
        }
        network_mode = "host"
      }

      resources {
        network {
          port "api" {
            static = 4001
          },
          port "webui" {
            static = 5001
          },
          port "http" {
            static = 8080
          }
        }
      }

      service {
        name = "ipfs"
        tags = ["urlprefix-/"]
        port = "webui"
        check {
          type     = "http"
          interval = "30s"
          timeout  = "5s"
          path = "/webui"
        }
      }
    }

    task "ipfs-cluster-service" {
      driver = "docker"

      env {
        IPFS_CLUSTER_CONSENSUS = "crdt"
        CLUSTER_SECRET = "617bcbdf3f62b9d5833f532d03d9fa95459e09d93577c22e478c98da3184f4f6"
        CLUSTER_RESTAPI_HTTPLISTENMULTIADDRESS = "/ip4/0.0.0.0/tcp/9094"
      } 


      config {
        image   = "ipfs/ipfs-cluster" 
        network_mode = "host"
        port_map {
          swarm = "9096"
          api = "9094"
          proxy = "9095"
        }
        volumes = [
          "/opt/ipfs_cluster_data:/data/ipfs-cluster"
        ]
      }

      resources {
        network {
          port "swarm" {
            static = 9096
          },
          port "api" {
            static = 9094
          },
          port "proxy" {
            static = 9095
          }
        }
      }

      service {
        name = "ipfs-cluster"
        tags = ["urlprefix-/cluster"]
        port = "api"
        check {
          type     = "http"
          interval = "30s"
          timeout  = "5s"
          path = "/health/graph"
        }
        /**connect {
          sidecar_service {
            proxy {
              upstreams {
                destination_name = "ethereum"
                local_bind_port  = 8546
              }
            }
          }
        }**/ 
      }
      template {
        change_mode = "noop"
        destination = "/opt/ipfs_cluster_data/peerstore"
        data = <<EOH
/dns4/ipfs-cluster.service.consul/tcp/9096/ipfs/QmcQ5XvrSQ4DouNkQyQtEoLczbMr6D9bSenGy6WQUCQUBt
EOH
      }
    }
  }
}