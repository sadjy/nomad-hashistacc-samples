job "ipfs" {
  datacenters = ["dc1","dc2"]
  region = "global"

  group "go-ipfs" {
    count = 1
    task "ipfs-node" {
      driver = "docker"

      config {
        image   = "ipfs/go-ipfs" 
        volumes = [
          "/opt/ipfs_exports:/export",
          "/opt/ipfs_data:/data/ipfs"
        ]
        port_map {
          api = "4001"
          webui = "5001"
          http = "8080"
        }
      }

      resources {
        network {
          port "api" {
            static = 4001
          },
          port "webui" {
            static = 5001
          },
          port "http" {
            static = 8080
          }
        }
      }

      service {
        name = "ipfs"
        tags = ["urlprefix-/"]
        port = "webui"
        check {
          type     = "http"
          interval = "30s"
          timeout  = "5s"
          path = "/webui"
        }
      }
    }
  }
}